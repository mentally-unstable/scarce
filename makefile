# hello
### https://www.cs.swarthmore.edu/~newhall/unixhelp/howto_makefiles.html ###

CC := gcc
CFLAGS := -Wall -Wextra
MAIN := scc

VPATH := src:src/

OBJ_DIR := src/obj
_OBJS := main.o
OBJS := $(patsubst %, $(OBJ_DIR)/%, $(_OBJS) )

.PHONY: depend clean

all: $(MAIN)
	@echo  \>\>\> \`$(MAIN)\' has been compiled \(./$(MAIN) to run\)

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(MAIN)

$(OBJ_DIR)/%.o : %.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@


clean:
	$(RM) $(OBJ_DIR)/*.o *~

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# DO NOT DELETE
