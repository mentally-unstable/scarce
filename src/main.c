/* hello */
#include <stdio.h>
#include <assert.h>

static char *shift( int *argc, char ***argv );

int main( int argc, char **argv ) {
    ;
}

/*
 * basically creates an iterable sequence
 * out of command line arguments
 */
static char *shift( int *argc, char ***argv ) {
    assert( *argc > 0 );
    char *result = **argv;
    *argv += 1;
    *argc -= 1;
    return result;
}

